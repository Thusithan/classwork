public class Naturalnumber{

    public static void main(String[] args) {

       int num = 10, i, sum = 0;

       for(i = 1; i <= num; i++){
           sum = sum + i;
       }

       System.out.println("Sum of first 10 natural numbers is: "+ sum);
    }
}